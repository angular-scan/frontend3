import { Injectable, Inject, InjectionToken, Optional } from '@angular/core';
import { TesteSession } from './teste-session';


export const MAT_DIALOG_DATA = new InjectionToken<any>('MatDialogData')

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(@Optional() @Inject(MAT_DIALOG_DATA) public scope: any) {
    // super(enume);
  }
}
