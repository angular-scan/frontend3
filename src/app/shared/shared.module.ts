import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';

import { MatRippleModule } from '@angular/material/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenuAsideComponent } from './components/menu-aside/menu-aside.component';

import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [HeaderComponent, MenuAsideComponent],
  imports: [
    CommonModule,
    MatRippleModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatIconModule,
  ],
  exports: [
    HeaderComponent,
    MatRippleModule,
    FormsModule,
    ReactiveFormsModule,
    MenuAsideComponent,
  ],
})
export class SharedModule {}
