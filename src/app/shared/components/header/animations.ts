import {
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes,
} from '@angular/animations';

export const markedTrigger = trigger('markedState', [
  state(
    'default',
    style({
      transform: 'translateX(0)',
      opacity: 1,
    })
  ),
  state(
    'marked',
    style({
      transform: 'translateX(100%)',
      opacity: 0,
    })
  ),
  transition('default => marked', [
    // style({
    //   border: '2px solid black',
    //   padding: '19px'
    // }),
    animate(
      '500ms ease-out',
      style({
        transform: 'translateX(100%)',
        opacity: 0,
      })
    ),
    animate(200),
  ]),
  transition('marked => default', [
    style({
      transform: 'translateX(-100%)',
      opacity: 0,
    }),
    animate('600ms ease-out', style({
      transform: 'translateX(0)',
      opacity: 1,
    })),
  ]),
  // transition('marked => default', animate('300ms ease-out')),
]);

export const itemStateTrigger = trigger('itemState', [
  transition(':enter', [
    animate(
      '500ms ease-out',
      keyframes([
        style({
          opacity: 0,
          transform: 'translateX(-100%)',
          offset: 0,
        }),
        style({
          opacity: 1,
          transform: 'translateX(15%)',
          offset: 0.4,
        }),
        style({
          opacity: 1,
          transform: 'translateX(0)',
          offset: 1,
        }),
      ])
    ),
  ]),
  transition(':leave', [
    animate(
      '500ms ease-in',
      keyframes([
        style({
          opacity: 1,
          transform: 'translateX(0)',
        }),
        style({
          transform: 'translateX(-15%)',
        }),
        style({
          opacity: 0,
          transform: 'translateX(100%)',
        }),
      ])
    ),
  ]),
  transition('slidUp => slidDown', [
    style({
      transform: 'translateY(-100%)',
    }),
    animate(
      '300ms ease-out',
      style({
        transform: 'translateY(0)',
      })
    ),
  ]),
  transition('slidDown => slidUp', [
    style({
      transform: 'translateY(0)',
    }),
    animate(
      '300ms ease-out',
      style({
        transform: 'translateY(-100%)',
      })
    ),
  ]),
]);

export const slideStateTrigger = trigger('slideState', [
  transition(':enter', [
    style({
      transform: 'translateY(-100%)',
    }),
    animate(
      '300ms ease-out',
      style({
        transform: 'translateY(0)',
      })
    ),
  ]),
  transition(':leave', [
    style({
      transform: 'translateY(0)',
    }),
    animate(
      '300ms ease-out',
      style({
        transform: 'translateY(-100%)',
      })
    ),
  ]),
]);


export const fade = trigger('fade', [
  transition(':enter', [
    style({
      opacity: 0,
    }),
    animate(
      '300ms ease-out',
      style({
        opacity: 1,
      })
    ),
  ]),
  transition(':leave', [
    style({
      transform: 'translateY(0)',
    }),
    animate(
      '300ms ease-out',
      style({
        transform: 'translateY(-100%)',
      })
    ),
  ]),
]);
