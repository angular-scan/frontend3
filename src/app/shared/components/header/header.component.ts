import { Component, OnInit } from '@angular/core';
import { markedTrigger, fade } from './animations';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [ markedTrigger, fade ]
})
export class HeaderComponent implements OnInit {
  logged = false;
  animationLogin = false;
  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.animationLogin = true;

      setTimeout(() => {
      this.logged = true;

      }, 600);
    }, 1000);
  }

}
